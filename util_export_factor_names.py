# -*- coding: utf-8 -*-
"""
    批量导出滑坡因子文件至
"""
import pandas as pd
import arcpy

if __name__ == "__main__":
    # arcgis工具箱模式
    year = arcpy.GetParameter(0)
    saved_dir = arcpy.GetParameterAsText(1)
    saved_slope_settings_filename = "{0}\\slopeSettings_{1}.csv".format(saved_dir, year)

    # 代码模式
    # arcpy.env.workspace = "G:/DataForDoctorPaper/博士论文数据.gdb"
    # year = 2010
    # saved_slope_settings_filename = "./settings/slopeSettings.csv"

    static_rasters = arcpy.ListDatasets("Factor_*", "Raster")
    cd_rasters = arcpy.ListDatasets("Change*", "Raster")
    current_cd_raster = None
    for nn in cd_rasters:
        if year == int(nn[-4:]):
            current_cd_raster = nn
            break
    landuse_raster = "LU_{0}".format(year)
    rain_raster = "Rain_{0}".format(year)
    roadbuffer_raster = "RoadBuffer_{0}".format(year)
    ndvi_raster = "NDVI_{0}".format(year)
    yLabel_raster = "y_isLandslide_{0}".format(year)

    factor_list = static_rasters.copy()
    factor_list.append(current_cd_raster)
    factor_list.append(landuse_raster)
    factor_list.append(rain_raster)
    factor_list.append(roadbuffer_raster)
    factor_list.append(ndvi_raster)
    factor_list.append(yLabel_raster)

    df = pd.DataFrame(factor_list)
    df.columns = ['factors']
    # print(df.columns)
    df.to_csv(saved_slope_settings_filename, encoding="gb2312", index=False)
    arcpy.AddMessage("Save file '{0}' successful!".format(saved_slope_settings_filename))
