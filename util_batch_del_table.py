# -*- coding: utf-8 -*-
"""
    批量删除要素文件
"""
import arcpy

def batDelFcClass(wildcard_str = "ZonalSt_*"):
    if wildcard_str is None or wildcard_str == "":
        arcpy.AddMessage("wildcard cannot be empty!")
        return None

    data_lst = arcpy.ListTables(wildcard_str)
    for dd in data_lst:
        # arcpy.AddMessage("Delete {0} starting...".format(dd))
        arcpy.Delete_management(dd)
        arcpy.AddMessage("Delete {0} successful!".format(dd))

if __name__ == "__main__":
    # arcgis工具箱模式
    wildcard_str = arcpy.GetParameterAsText(0)
    wildcard_str = wildcard_str.strip()
    batDelFcClass(wildcard_str)

    # # 代码模式
    # arcpy.env.workspace = "G:/DataForDoctorPaper/博士论文数据.gdb"
    # wildcard_str = "ZonalSt_*"
    # batDelFcClass(wildcard_str)
