# -*- coding: GBK -*-
"""
根据【年份】参数，
利用【多值提取至点】工具，将各个因子提取至栅格单元矢量点

"""
import sys
import arcpy

def exactValueToPt_by_prefixYear(prefix=None, year=None):
    """
        根据年份和前缀提取因子的值是栅格单元点
    :param prefix: str
        前缀字符串
    :param year: int
        年份
    :return:
        提取成功返回True，否则返回False
    """
    if prefix is None or year is None:
        return False

    factor_raster_str = "{0}{1}".format(prefix,year)
    if arcpy.Exists(factor_raster_str):
        factor_field_str = "{0} {1}".format(factor_raster_str, factor_raster_str)
        arcpy.sa.ExtractMultiValuesToPoints(input_fc, factor_field_str, "NONE")
        return True

def exact_factors(input_fc='RasterUnitsV4_2003',
                  year=2003,
                  del_exist_fields=True):
    #Step 0： 相关参数
    static_rasters = arcpy.ListDatasets("Factor_*", "Raster")

    # 删除已经存在的字段
    if del_exist_fields:
        field_obj_list = arcpy.ListFields(input_fc)
        field_list = [f.name for f in field_obj_list]
        field_list.remove("OBJECTID")
        field_list.remove("Shape")
        del_field_str = ";".join(field_list)
        print(del_field_str)
        if del_field_str is not None and len(del_field_str)!=0:
            arcpy.management.DeleteField(input_fc, del_field_str)

    #Step 1：提取静态因子, situation 1和situation 2，二选一
    # situation 1:
    # x_field_names = ["X"+str(seq+1) for seq in range(len(static_rasters))]
    # ziped_fields_list = list(zip(static_rasters, x_field_names))
    # rasters_fields = [src_name +" " + t_name for src_name, t_name in ziped_fields_list]

    # situation 2:
    rasters_fields = [x +" " + x for x in static_rasters]

    rasters_fields_str = ";".join(rasters_fields)
    # print(rasters_fields_str)
    if rasters_fields_str is not None or rasters_fields_str!="":
        print("Execute ExtractMultiValuesToPoints:")
        arcpy.sa.ExtractMultiValuesToPoints(input_fc, rasters_fields_str, "NONE")

    #Step 2： 提取土地利用信息
    #try:
    exactValueToPt_by_prefixYear(prefix="LU_", year=year)
    # except Exception:
        # e = sys.exc_info()[1]
        # arcpy.AddMessage(e.args[0])

    #Step 3： 提取降雨因子
    exactValueToPt_by_prefixYear(prefix="Rain_", year=year)

    #Step 4：提取土地利用变化因子
    cd_rasters_name = arcpy.ListDatasets("Change*", "Raster")
    current_cd_raster_name = None
    for nn in cd_rasters_name:
        if year == int(nn[-4:]):
            current_cd_raster_name = nn
            break
    if current_cd_raster_name is not None:
        cd_field_str = "{0} {1}".format(current_cd_raster_name, current_cd_raster_name)
        arcpy.sa.ExtractMultiValuesToPoints(input_fc, cd_field_str, "NONE")
    print(current_cd_raster_name)

    # Step 5： 提取道路缓冲区列
    exactValueToPt_by_prefixYear(prefix="RoadBuffer_", year=year)

    # Step 6： 提取NDVI值
    exactValueToPt_by_prefixYear(prefix="NDVI_", year=year)

    #Step 7：标签列
    exactValueToPt_by_prefixYear(prefix="y_isLandslide_", year=year)

if __name__=="__main__":
    # Set the current workspace
    #arcpy.env.workspace = "G:/DataForDoctorPaper/博士论文数据.gdb"
    input_fc=arcpy.GetParameterAsText(0)
    year = int(arcpy.GetParameterAsText(1))
    is_del_exist_fields = arcpy.GetParameter(2)
    exact_factors(input_fc=input_fc,
                  year=year,
                  del_exist_fields=is_del_exist_fields)

    # input_fc = 'RasterUnitsV4_2003'
    # rain_raster_str = "Rain_{0}".format(2003)
    # rain_field_str = "{0} {1}".format(rain_raster_str, rain_raster_str)
    # try:
    #     arcpy.sa.ExtractMultiValuesToPoints(input_fc, rain_field_str, "NONE")
    # except arcpy.ExecuteError:
    #     arcpy.AddError(arcpy.GetMessages(2))
    #     arcpy.AddMessage("Continue Next Steps!")