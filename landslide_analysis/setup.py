# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name='landslideAnalysis',  # 应用名
    author = "Xu Shiluo",
    author_email = "xushiluo@163.com",
    version='0.1',
    license = "BSD",
    packages=['landslide_preprocessing']
)