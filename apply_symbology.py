# -*- coding: utf-8 -*-
import arcpy

# Step 1: 应用栅格样式文件到预测结果
# output_raster_fullpath = arcpy.env.workspace + "/" + output_raster_name
# print(output_raster_fullpath)
cur_raster_name=None
raster_style_fullepath = r"G:\DataForDoctorPaper\Z01_Styles\栅格样式\栅格-栅格单元-logistic-预测结果样式.lyr"

# tmp_raster_name = "tmp_" + cur_raster_name
# arcpy.MakeRasterLayer_management(output_raster_name, tmp_raster_name)
arcpy.management.ApplySymbologyFromLayer(cur_raster_name,
                                         raster_style_fullepath,
                                         None,
                                         "DEFAULT")
print("Apply Symbology to '{0}' success!\n".format(cur_raster_name))

if __name__ == "__main__":
    pass