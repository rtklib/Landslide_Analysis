# -*- coding: utf-8 -*-
import arcpy
from table_to_csv import table_to_csv

def removeID_column(lst):
    """
        去除了lst列表中含有字符_OBJECTID和_ID_的元素

    :param lst:list
        输入列表

    :return:
        去除相关元素后的列表

    """
    new_lst = lst.copy()
    for l in lst:
        if l.endswith("_OBJECTID"):
            new_lst.remove(l)
        if l.find("_ID_") != -1:
            new_lst.remove(l)
        if l.startswith("ID_"):
            new_lst.remove(l)
    return new_lst


if __name__ == "__main__":
    input_fc = arcpy.GetParameterAsText(0)
    year = int(arcpy.GetParameterAsText(1))
    export_fields = arcpy.GetParameterAsText(2)
    csv_file_path = arcpy.GetParameterAsText(3)
    export_fields = export_fields.strip()
    csv_file_path = csv_file_path.strip()

    table_to_csv(input_fc=input_fc, year=year, sel_fields=export_fields, saved_filename=csv_file_path,
                 additional_process_fun=removeID_column)



    # # 代码模式下，需要设定工作区间
    # arcpy.env.workspace = "G:/DataForDoctorPaper/博士论文数据.gdb"
    # export_fields = "ID_地震点;地震点_MEAN;ID_Aspect;Aspect_MEAN;y_isLandslide_2010_MAJORITY"
    # csv_file_path = "./results/table_to_csv_slope.csv"
    # table_to_csv(input_fc='slopeUnits_V5_2010', year=2010, sel_fields=export_fields, saved_filename=csv_file_path,
    #              additional_process_fun=removeID_column)
