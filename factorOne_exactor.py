# -*- coding: GBK -*-
"""
根据【年份】参数，
利用【多值提取至点】工具，将各个因子提取至栅格单元矢量点

"""
import sys
import arcpy

def exact_currentFactor(factor_raster_str, input_fc='RasterUnitsV4_2003', del_exist_fields=False):

    # 删除已经存在的字段
    if del_exist_fields:
        field_obj_list = arcpy.ListFields(input_fc)
        field_list = [f.name for f in field_obj_list]
        field_list.remove("OBJECTID")
        field_list.remove("Shape")
        del_field_str = ";".join(field_list)
        print(del_field_str)
        if del_field_str is not None and len(del_field_str)!=0:
            arcpy.management.DeleteField(input_fc, del_field_str)

    #Step 1： 提取当前栅格图层因子
    if arcpy.Exists(factor_raster_str):
        factor_field_str = "{0} {1}".format(factor_raster_str, factor_raster_str)
        arcpy.sa.ExtractMultiValuesToPoints(input_fc, factor_field_str, "NONE")


if __name__=="__main__":
    # Set the current workspace
    #arcpy.env.workspace = "G:/DataForDoctorPaper/博士论文数据.gdb"
    input_fc=arcpy.GetParameterAsText(0)
    factor_layer_name = arcpy.GetParameterAsText(1)
    is_del_exist_fields = arcpy.GetParameter(2)
    exact_currentFactor(factor_raster_str= factor_layer_name,
                        input_fc=input_fc,
                        del_exist_fields=is_del_exist_fields)
