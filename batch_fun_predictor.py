# -*- coding: utf-8 -*-
"""
    批量执行滑坡灾害危险性评估，以代码模式运行，不做成ArcGIS工具箱

"""
from fun_predictor import  execute_prediction

if __name__ == "__main__":

        # 相关参数设置
        data_ver = "V5"
        clf_name = "svm"
        process_unit = 'slope'
        label_prefix = 'y_isLandslide_'
        GeoID_name = "OBJECTID"

        iter_lst = [('slopeUnits_V5_2001.csv',2001),
                    ('slopeUnits_V5_2003.csv',2003),
                    ('slopeUnits_V5_2006.csv',2006),
                    ('slopeUnits_V5_2008.csv',2008),
                    ('slopeUnits_V5_2010.csv',2010),
                    ('slopeUnits_V5_2013.csv',2013),
                    ('slopeUnits_V5_2015.csv',2015),
                    ('slopeUnits_V5_2017.csv',2017)]



        result_lst = []
        for inp_csv,year in iter_lst:
              input_csv = "./results/{0}".format(inp_csv)
              is_success, accuracy, auc_val, recall_val = execute_prediction(data_filename=input_csv,
                                                                             year=year,
                                                                             GeoID_name=GeoID_name,
                                                                             label_prefix=label_prefix,
                                                                             data_version=data_ver,
                                                                             clf_name=clf_name,
                                                                             process_unit=process_unit,
                                                                             is_plot_roc=True,
                                                                             test_percent=0.3)
              result_lst.append((accuracy,auc_val, recall_val))

        print("ACC\tAUC\tRecall")
        for re in result_lst:
           print("{0:.3f}\t{1:.3f}\t{2:.3f}".format(re[0], re[1], re[2]))