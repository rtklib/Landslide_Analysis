# -*- coding: utf-8 -*-

import arcpy

def batch_del_fields(input_fc, prefix):
    """
        批量删除字段，字段名称以变量prefix字符为起始

    :param input_fc: str
        输入要素

    :param prefix: str
        起始字符串

    :return: list or None
        没有删除任何字段返回None；否则返回删除的字段列表

    """
    tmp_fields = arcpy.ListFields(input_fc)
    tmp_field_names_lst = [f.name for f in tmp_fields]
    del_lst = []
    for tf in tmp_field_names_lst:
        if tf.startswith(prefix):
            del_lst.append(tf)

    if len(del_lst) > 0:
        del_re = arcpy.DeleteField_management(input_fc, del_lst)
        if del_re.status == 4:
            arcpy.AddMessage("Delete field: {0} successful!".format(del_lst))
            return del_lst

    return None


if __name__ == "__main__":
    # ArcGIS工具箱模式
    input_fc = arcpy.GetParameterAsText(0)
    prefix = arcpy.GetParameterAsText(1)

    # 代码模式
    # arcpy.env.workspace = "G:/DataForDoctorPaper/博士论文数据.gdb"
    # input_fc = "slopeUnits_V5_2017"
    # prefix = "ID_"

    batch_del_fields(input_fc, prefix)




