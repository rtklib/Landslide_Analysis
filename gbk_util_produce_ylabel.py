# -*- coding: cp936 -*-
import arcpy
# this tool execute in ArcMap 10.6

fc_study_area = arcpy.GetParameterAsText(0)
fc_landslide = arcpy.GetParameterAsText(1)
year = arcpy.GetParameter(2)
cell_size = float(arcpy.GetParameterAsText(3))
y_field_prefix = arcpy.GetParameterAsText(4)
val_landslide = float(arcpy.GetParameterAsText(5))
val_notlandslide = float(arcpy.GetParameterAsText(6))



y_field_name = "{0}{1}".format(y_field_prefix,year)
fc_symdiff = "tmp_symdiff_{0}".format(year)
fc_merge = "tmp_merge_{0}".format(year)
raster_output_label = "{0}".format(y_field_name)

# delete exist temporary feature class
if arcpy.Exists(fc_symdiff):
    arcpy.Delete_management(fc_symdiff)
if arcpy.Exists(fc_merge):
    arcpy.Delete_management(fc_merge)
if arcpy.Exists(raster_output_label):
    arcpy.Delete_management(raster_output_label)

#---------------------------------------------------
arcpy.analysis.SymDiff(fc_study_area, fc_landslide, fc_symdiff, "NO_FID", None)

arcpy.management.AddField(fc_symdiff, y_field_name, "LONG", None, None, None, y_field_name,
                          "NULLABLE", "NON_REQUIRED", None)

non_landslide_exp = "{0}".format(val_notlandslide)
arcpy.CalculateField_management(in_table=fc_symdiff,
                                field=y_field_name,
                                expression=non_landslide_exp,
                                expression_type="PYTHON")

#----------------------------------------------------

result_landslide_add = arcpy.AddField_management(fc_landslide, y_field_name, "LONG", None, None, None, y_field_name,
                                                 "NULLABLE", "NON_REQUIRED", None)


landslide_exp = "{0}".format(val_landslide)
arcpy.CalculateField_management(in_table=fc_landslide,
                                field=y_field_name,
                                expression=landslide_exp,
                                expression_type="PYTHON")


fieldMappings = arcpy.FieldMappings()

fldMap = arcpy.FieldMap()
fldMap.mergeRule = 'Last'
fldMap.addInputField(fc_symdiff, y_field_name)
fldMap.addInputField(fc_landslide, y_field_name)

label_name = fldMap.outputField
label_name.name = y_field_name
fldMap.outputField = label_name

fieldMappings.addFieldMap(fldMap)

result_merge = arcpy.Merge_management([fc_symdiff,fc_landslide], fc_merge, fieldMappings)

#----------------------------------------------------
y_val_field = y_field_name
result_cov = arcpy.conversion.PolygonToRaster(fc_merge, y_val_field, raster_output_label,
                                              "CELL_CENTER", "NONE", cell_size)


if arcpy.Exists(fc_symdiff):
    arcpy.Delete_management(fc_symdiff)
if arcpy.Exists(fc_merge):
    arcpy.Delete_management(fc_merge)

re=arcpy.DeleteField_management(fc_landslide, y_field_name)